package br.com.a0.david2.gamessale.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Bruno Alcides on 17/10/2017.
 */

public class Leilao implements Serializable {

    @SerializedName("Codigo")
    private int codigo;

    @SerializedName("Titulo")
    private String titulo;

    @SerializedName("Descricao")
    private String descricao;

    @SerializedName("Categoria")
    private int categoriaId;

    @SerializedName("CategoriaModel")
    private Categoria categoria;

    @SerializedName("Plataforma")
    private int plataformaId;

    @SerializedName("PlataformaModel")
    private Plataforma plataforma;

    @SerializedName("Usuario")
    private int usuarioId;

    @SerializedName("UsuarioModel")
    private Usuario usuario;

    @SerializedName("UsuarioVencedor")
    private int usuarioVencedorId;

    @SerializedName("UsuarioVencedorModel")
    private Usuario usuarioVencedor;

    @SerializedName("Lance")
    private double lance;

    public Leilao() {}

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(int categoriaId) {
        this.categoriaId = categoriaId;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public int getPlataformaId() {
        return plataformaId;
    }

    public void setPlataformaId(int plataformaId) {
        this.plataformaId = plataformaId;
    }

    public Plataforma getPlataforma() {
        return plataforma;
    }

    public void setPlataforma(Plataforma plataforma) {
        this.plataforma = plataforma;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public int getUsuarioVencedorId() {
        return usuarioVencedorId;
    }

    public void setUsuarioVencedorId(int usuarioVencedorId) {
        this.usuarioVencedorId = usuarioVencedorId;
    }

    public Usuario getUsuarioVencedor() {
        return usuarioVencedor;
    }

    public void setUsuarioVencedor(Usuario usuarioVencedor) {
        this.usuarioVencedor = usuarioVencedor;
    }

    public double getLance() {
        return lance;
    }

    public void setLance(double lance) {
        this.lance = lance;
    }
}
