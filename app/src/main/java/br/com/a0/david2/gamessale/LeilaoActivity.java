package br.com.a0.david2.gamessale;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import br.com.a0.david2.gamessale.Models.Categoria;
import br.com.a0.david2.gamessale.Models.Leilao;
import br.com.a0.david2.gamessale.Models.Plataforma;
import br.com.a0.david2.gamessale.Models.Usuario;
import br.com.a0.david2.gamessale.Rest.APIClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeilaoActivity extends AppCompatActivity {

    private Leilao leilao;
    private Toolbar toolbar;
    private ImageView imgJogo;
    private TextView txtTitulo;
    private EditText edtTitulo;
    private TextView txtDescricao;
    private EditText edtDescricao;
    private TextView txtLance;
    private TextView txtCategoria;
    private Spinner spnCategoria;
    private TextView txtPlataforma;
    private Spinner spnPlataforma;
    private Button btnSalvar;
    private ImageButton btnVoltar;
    private ImageButton btnEditLeilao;
    private ImageButton btnCancelarEdit;

    private Usuario usuarioLogado;

    private boolean isEditing = false;
    private int indicePlataformaSelecionada = 0;
    private int indiceCategoriaSelecionada = 0;

    public void pegaElementos() {
        btnVoltar = (ImageButton) findViewById(R.id.btnVoltar);
        btnEditLeilao = (ImageButton) findViewById(R.id.btnEditLeilao);
        btnCancelarEdit = (ImageButton) findViewById(R.id.btnCancelaEdit);
        btnSalvar = (Button) findViewById(R.id.btnSalvar);

        imgJogo = (ImageView) findViewById(R.id.imgJogo);

        txtTitulo = (TextView) findViewById(R.id.txtTitulo);
        txtDescricao = (TextView) findViewById(R.id.txtDescricao);
        txtLance = (TextView) findViewById(R.id.txtLance);
        txtCategoria = (TextView) findViewById(R.id.txtCategoria);
        txtPlataforma = (TextView) findViewById(R.id.txtPlataforma);

        edtTitulo = (EditText) findViewById(R.id.edtTitulo);
        edtDescricao = (EditText) findViewById(R.id.edtDescricao);
        spnCategoria = (Spinner) findViewById(R.id.spnCategoria);
        spnPlataforma = (Spinner) findViewById(R.id.spnPlataforma);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Picasso.with(getApplicationContext())
                .load("http://igg-games.com/wp-content/uploads/2015/08/Megaman-X-Complete-PC-Collection-Free-Download.jpg")
                .resize(imgJogo.getWidth(), 300)
                .into(imgJogo);
    }

    public void pegaUsuarioLogado() {
        String spKey = getString(R.string.perf_key);
        SharedPreferences sp = getSharedPreferences(spKey, MODE_PRIVATE);

        String usuario = sp.getString("usuarioLogado", null);

        if (usuario != null) {
            usuarioLogado = new Gson().fromJson(usuario, Usuario.class);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leilao);

        pegaElementos();
        pegaUsuarioLogado();
        pegaLeilaoSelecionado();
        adicionaEventoAosBotoesDoHeader();
        controlaVisibilidade();
        carregaSpinnerPlataformas();
        carregaSpinnerCategorias();

        Picasso.with(getApplicationContext())
                .load("http://igg-games.com/wp-content/uploads/2015/08/Megaman-X-Complete-PC-Collection-Free-Download.jpg")
                .resize(imgJogo.getWidth(), 360)
                .into(imgJogo);
    }

    public void adicionaEventoAosBotoesDoHeader() {
        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnCancelarEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEditing = false;
                controlaVisibilidade();
            }
        });

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salvarLeilao();
            }
        });

        btnEditLeilao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEditing = true;
                controlaVisibilidade();
            }
        });
    }

    public void pegaLeilaoSelecionado() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            leilao = (Leilao) getIntent().getSerializableExtra("leilaoSelecionado"); //Obtaining data
        }
    }

    public void controlaVisibilidade() {
        findViewById(R.id.lanceContainer).setVisibility(isEditing ? View.GONE : View.VISIBLE);

        setTextViewVisibility();
        setEditTextVisibility();

        if (isEditing) {
            btnEditLeilao.setVisibility(View.GONE);
            btnCancelarEdit.setVisibility(View.VISIBLE);
            btnSalvar.setVisibility(View.VISIBLE);

            setEditTextValues();
        } else {
            boolean criadoPorUsuarioLogado = leilao.getUsuario().getCodigo() == usuarioLogado.getCodigo();

            btnEditLeilao.setVisibility(criadoPorUsuarioLogado ? View.VISIBLE : View.GONE);
            btnCancelarEdit.setVisibility(View.GONE);
            btnSalvar.setVisibility(View.GONE);

            setTextViewValues();
        }
    }

    public void setTextViewVisibility() {
        int visibility = isEditing ? View.GONE : View.VISIBLE;

        txtTitulo.setVisibility(visibility);
        txtDescricao.setVisibility(visibility);
        txtLance.setVisibility(visibility);
        txtCategoria.setVisibility(visibility);
        txtPlataforma.setVisibility(visibility);
    }

    public void setEditTextVisibility() {
        int visibilty = isEditing ? View.VISIBLE : View.GONE;

        edtTitulo.setVisibility(visibilty);
        edtDescricao.setVisibility(visibilty);
        spnCategoria.setVisibility(visibilty);
        spnPlataforma.setVisibility(visibilty);
    }

    public void setTextViewValues() {
        String lanceFormatado = String.format("%.2f", leilao.getLance());
        String lance = "R$ " + lanceFormatado.replace('.', ',');

        txtTitulo.setText(leilao.getTitulo());
        txtDescricao.setText(leilao.getDescricao());
        txtLance.setText(lance);
        txtCategoria.setText(leilao.getCategoria().getTitulo());
        txtPlataforma.setText(leilao.getPlataforma().getTitulo());
    }

    public void setEditTextValues() {
        edtTitulo.setText(leilao.getTitulo());
        edtDescricao.setText(leilao.getDescricao());
        spnCategoria.setSelection(indiceCategoriaSelecionada);
        spnPlataforma.setSelection(indicePlataformaSelecionada);
    }

    public void carregaSpinnerPlataformas() {
        Call<List<Plataforma>> call = APIClient.getPlataformaClient().getPlataformas();
        call.enqueue(new Callback<List<Plataforma>>() {
            @Override
            public void onResponse(Call<List<Plataforma>> call, Response<List<Plataforma>> response) {
                if (response.isSuccessful()) {
                    List<Plataforma> plataformas = response.body();

                    for (int i = 0; i < plataformas.size(); i++) {
                        String tituloSelecionado = leilao.getPlataforma().getTitulo();
                        Plataforma plataforma = plataformas.get(i);

                        if (plataforma.getTitulo().equals(tituloSelecionado)) {
                            indicePlataformaSelecionada = i;
                            break;
                        }
                    }

                    ArrayAdapter<Plataforma> adapter = new ArrayAdapter<>(LeilaoActivity.this, android.R.layout.simple_spinner_item, plataformas);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spnPlataforma.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<Plataforma>> call, Throwable t) {
                Log.d("MainActivity", "Status Code = " + t);
            }
        });
    }

    public void carregaSpinnerCategorias() {
        Call<List<Categoria>> call = APIClient.getCategoriaClient().getCategorias();
        call.enqueue(new Callback<List<Categoria>>() {
            @Override
            public void onResponse(Call<List<Categoria>> call, Response<List<Categoria>> response) {
                if (response.isSuccessful()) {
                    List<Categoria> categorias = response.body();

                    for (int i = 0; i < categorias.size(); i++) {
                        String tituloSelecionado= leilao.getCategoria().getTitulo();
                        Categoria categoria = categorias.get(i);

                        if (categoria.getTitulo().equals(tituloSelecionado)) {
                            indiceCategoriaSelecionada = i;
                            break;
                        }
                    }

                    ArrayAdapter<Categoria> adapter = new ArrayAdapter<>(LeilaoActivity.this, android.R.layout.simple_spinner_item, categorias);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spnCategoria.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<Categoria>> call, Throwable t) {
                Log.d("MainActivity", "Status Code = " + t);
            }
        });
    }

    public void salvarLeilao() {
        int leilaoId = leilao.getCodigo();

        Categoria categoriaSelecionada = (Categoria) spnCategoria.getSelectedItem();
        Plataforma plataformaSelecionada = (Plataforma) spnPlataforma.getSelectedItem();
        Leilao novoLeilao = new Leilao();

        novoLeilao.setTitulo(edtTitulo.getText().toString());
        novoLeilao.setDescricao(edtDescricao.getText().toString());
        novoLeilao.setCategoriaId(categoriaSelecionada.getCodigo());
        novoLeilao.setPlataformaId(plataformaSelecionada.getCodigo());

        Call<Leilao> call = APIClient.getLeilaoClient().atualizaLeilao(leilaoId, novoLeilao);
        call.enqueue(new Callback<Leilao>() {
            @Override
            public void onResponse(Call<Leilao> call, Response<Leilao> response) {
                if (response.isSuccessful()) {
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Leilao> call, Throwable t) {
                Log.d("MainActivity", "Status Code = " + t);
            }
        });
    }
}
