package br.com.a0.david2.gamessale.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import br.com.a0.david2.gamessale.Models.Usuario;

/**
 * Created by Bruno Alcides on 17/10/2017.
 */

public class UsuarioDAO extends SQLiteOpenHelper {
    private static final String BANCO = "GAMES_SALE";
    private static final String TABELA = "USUARIOS";
    private static final int VERSAO = 1;

    public UsuarioDAO(Context context) {
        super(context, BANCO, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABELA + " (" +
            "CODIGO INTEGER, NOME TEXT, EMAIL TEXT, SENNHA TEXT, TIPO INTEGER)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS " + TABELA;
        db.execSQL(sql);
        onCreate(db);
    }

    @NonNull
    private ContentValues getContentValues(Usuario usuario) {
        ContentValues valores = new ContentValues();
        valores.put("CODIGO", usuario.getCodigo());
        valores.put("NOME", usuario.getNome());
        valores.put("EMAIL", usuario.getEmail());
        valores.put("SENHA", usuario.getSenha());
        valores.put("TIPO", usuario.getTipo());
        return valores;
    }

    public Usuario getById(String codigoDoUsuario) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABELA + " WHERE codigo = ?", new String[] {codigoDoUsuario});

        if (cursor.moveToFirst()) {
            int codigo = cursor.getInt(0);
            String nome = cursor.getString(1);
            String email = cursor.getString(2);
            String senha = cursor.getString(3);
            int tipo = cursor.getInt(4);

            Usuario usuario = new Usuario();

            usuario.setCodigo(codigo);
            usuario.setNome(nome);
            usuario.setEmail(email);
            usuario.setEmail(senha);
            usuario.setTipo(tipo);

            return usuario;
        }

        return null;
    }

    public void insert(Usuario usuario){
        ContentValues valores = getContentValues(usuario);
        getWritableDatabase().insert(TABELA, null, valores);
    }

    public void update(Usuario usuario){
        ContentValues valores = getContentValues(usuario);
        getWritableDatabase().update(TABELA, valores,
                "CODIGO = ?", new String[]{String.valueOf(usuario.getCodigo())});
    }

    public void delete(int codigo){
        getWritableDatabase().delete(TABELA,
                "CODIGO = ?", new String[]{String.valueOf(codigo)});
    }

    public List<Usuario> list(){
        Cursor cursor = getReadableDatabase()
                .query(TABELA, null, null, null, null, null, null);
        List<Usuario> produtos = new ArrayList<>();

        while (cursor.moveToNext()){
            int codigo = cursor.getInt(0);
            String nome = cursor.getString(1);
            String email = cursor.getString(2);
            String senha = cursor.getString(3);
            int tipo = cursor.getInt(4);

            Usuario usuario = new Usuario();

            usuario.setCodigo(codigo);
            usuario.setNome(nome);
            usuario.setEmail(email);
            usuario.setEmail(senha);
            usuario.setTipo(tipo);
            produtos.add(usuario);
        }
        return produtos;
    }
}
