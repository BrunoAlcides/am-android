package br.com.a0.david2.gamessale;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import br.com.a0.david2.gamessale.Models.Leilao;
import br.com.a0.david2.gamessale.Models.Usuario;
import br.com.a0.david2.gamessale.Rest.APIClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class MainActivity extends AppCompatActivity {
    private SwipeRefreshLayout refreshListLeiloes;
    private RecyclerView lstLeiloes;
    private List<Leilao> leiloes;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mandaParaLoginCasoNaoEstejaLogado();
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        lstLeiloes = (RecyclerView) findViewById(R.id.lstLeiloes);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        refreshListLeiloes = (SwipeRefreshLayout) findViewById(R.id.refreshListLeiloes);

        setSupportActionBar(toolbar);
        createRefresh();
        getLeiloes();
//        } else {
//            Type listType = new TypeToken<List<Leilao>>(){}.getType();
//            String json = savedInstanceState.getString("leiloes");
//
//            leiloes = new Gson().fromJson(json, listType);
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.act_deslogar) {
            deslogar();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void mandaParaLoginCasoNaoEstejaLogado() {
        if (!usuarioEstaLogado()) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }

    public boolean usuarioEstaLogado() {
        String spKey = getString(R.string.perf_key);
        SharedPreferences sp = getSharedPreferences(spKey, MODE_PRIVATE);

        return sp.getString("usuarioLogado", null) != null;
    }

    public void deslogar() {
        String spKey = getString(R.string.perf_key);
        SharedPreferences sp = getSharedPreferences(spKey, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.remove("usuarioLogado");
        editor.apply();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void getLeiloes() {
        getLeilaoViaWebService();
    }

    public void getLeilaoViaWebService() {
        Call<List<Leilao>> call = APIClient.getLeilaoClient().getLeiloes();
        refreshListLeiloes.setRefreshing(true);
        call.enqueue(new Callback<List<Leilao>>() {
            @Override
            public void onResponse(Call<List<Leilao>> call, Response<List<Leilao>> response) {
                if (response.isSuccessful()) {
                    leiloes = response.body();

                    lstLeiloes.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                    lstLeiloes.setAdapter(new LeiloesListAdapter(response.body(), MainActivity.this, new LeiloesListAdapter.onClickNovoLance() {
                        @Override
                        public void onClickNovoLance(Leilao leilao) {
                            abrirModalNovoLance(leilao);
                        }
                    }));
                    refreshListLeiloes.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<List<Leilao>> call, Throwable t) {
                Log.d("MainActivity", "Status Code = " + t);
            }
        });
    }

        public void createRefresh() {
        refreshListLeiloes.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLeilaoViaWebService();
            }
        });
    }

    private void abrirModalNovoLance(final Leilao leilao) {
        final AlertDialog dialog;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        View modal = inflater.inflate(R.layout.dar_lance_modal, null);

        final TextView txtNovoLance = (TextView) modal.findViewById(R.id.txtNovoLance);
        final SeekBar skbNovoLance = (SeekBar) modal.findViewById(R.id.skbNovoLance);
        Button btnCancelar = (Button) modal.findViewById(R.id.btnCancelar);
        Button btnDarNovoLance = (Button) modal.findViewById(R.id.btnDarNovoLance);
        final ImageButton btnMudaInput = (ImageButton) modal.findViewById(R.id.btnMudaInput);
        final EditText edtNovoLance = (EditText) modal.findViewById(R.id.edtNovoLance);

        int lance = (int) leilao.getLance();
        skbNovoLance.setMax(lance);
        String lanceFormatado = String.format("%.2f", leilao.getLance());
        edtNovoLance.setText(lanceFormatado);
        txtNovoLance.setText("R$ " + lanceFormatado.replace('.', ','));
        btnMudaInput.setTag(0);

        dialog = new AlertDialog.Builder(MainActivity.this).setView(modal).create();
        dialog.show();

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        skbNovoLance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String lanceFormatado = String.format("%.2f", (leilao.getLance() + progress));
                edtNovoLance.setText(lanceFormatado);
                String lance = "R$ " + lanceFormatado.replace('.', ',');
                txtNovoLance.setText(lance);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btnMudaInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSeekbar = ((int) btnMudaInput.getTag()) == 0;
                if (isSeekbar) {
                    txtNovoLance.setVisibility(GONE);
                    skbNovoLance.setVisibility(GONE);
                    edtNovoLance.setVisibility(VISIBLE);
                    btnMudaInput.setImageResource(R.drawable.ic_close_black_24dp);
                    btnMudaInput.setTag(1);
                } else {
                    txtNovoLance.setVisibility(VISIBLE);
                    skbNovoLance.setVisibility(VISIBLE);
                    edtNovoLance.setVisibility(GONE);
                    btnMudaInput.setImageResource(R.drawable.ic_space_bar_black_24dp);
                    btnMudaInput.setTag(0);
                }
            }
        });

        btnDarNovoLance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String spKey = getString(R.string.perf_key);
                SharedPreferences sp = getSharedPreferences(spKey, MODE_PRIVATE);
                Usuario usuarioLogado = new Usuario();

                usuarioLogado.setCodigo(0);

                String usuario = sp.getString("usuarioLogado", null);

                if (usuario != null) {
                    usuarioLogado = new Gson().fromJson(usuario, Usuario.class);
                }

                boolean isSeekbar = ((int) btnMudaInput.getTag()) == 0;
                double novoLance = isSeekbar ? leilao.getLance() + skbNovoLance.getProgress() : Double.parseDouble(edtNovoLance.getText().toString().replace(',', '.'));
                int leilaoId = leilao.getCodigo();
                int usuarioId = usuarioLogado.getCodigo();

                Call<Leilao> call = APIClient.getLeilaoClient().darNovoLance(leilaoId, usuarioId, novoLance);
                call.enqueue(new Callback<Leilao>() {
                    @Override
                    public void onResponse(Call<Leilao> call, Response<Leilao> response) {
                        if (response.isSuccessful()) {
                            getLeiloes();
                            dialog.dismiss();
                        } else {
                            try {
                                String messageJSON = response.errorBody().string();
                                JSONObject json = new JSONObject(messageJSON);
                                Toast.makeText(MainActivity.this, "ERRO: " + json.getString("Message"), Toast.LENGTH_LONG).show();
                            } catch (IOException | JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Leilao> call, Throwable t) {
                        Log.d("MainActivity", "Status Code = " + t);
                    }
                });
            }
        });
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
//        super(persistentState,persistentState)
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("leiloes", new Gson().toJson(leiloes));

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }
}
