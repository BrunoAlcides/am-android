package br.com.a0.david2.gamessale;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import br.com.a0.david2.gamessale.Models.Leilao;
import br.com.a0.david2.gamessale.Models.Usuario;
import br.com.a0.david2.gamessale.R;
import br.com.a0.david2.gamessale.Rest.APIClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static android.support.v7.widget.RecyclerView.*;

/**
 * Created by Bruno Alcides on 17/10/2017.
 */

public class LeiloesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public interface onClickNovoLance {
        void onClickNovoLance(Leilao leilao);
    }

    private List<Leilao> leiloes;
    private Context context;
    private final onClickNovoLance listener;


    public LeiloesListAdapter(List<Leilao> leiloes, Context context, onClickNovoLance listener) {
        this.leiloes = leiloes;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.leilao_item, parent, false);

        return new ListItem(row);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ListItem item = (ListItem) holder;
        Leilao leilao = leiloes.get(position);

        String lanceFormatado = String.format("%.2f", leilao.getLance());
        String lance = "R$ " + lanceFormatado.replace('.', ',');

        String criador = "Criador: " + leiloes.get(position).getUsuario().getNome();

        item.txtTitulo.setText(leilao.getTitulo());
        item.txtLance.setText(lance);
        item.txtCriador.setText(criador);

        Picasso.with(context)
                .load("http://igg-games.com/wp-content/uploads/2015/08/Megaman-X-Complete-PC-Collection-Free-Download.jpg")
                .resize(40, 40)
                .into(item.img);

        String spKey = context.getString(R.string.perf_key);
        SharedPreferences sp = context.getSharedPreferences(spKey, MODE_PRIVATE);
        Usuario usuarioLogado;

        String usuario = sp.getString("usuarioLogado", null);


        if (usuario != null) {
            usuarioLogado = new Gson().fromJson(usuario, Usuario.class);

            if (usuarioLogado.getCodigo() == leiloes.get(position).getUsuarioId()) {
                item.btnLance.setVisibility(INVISIBLE);
            }
        }

        item.bindOnClickListeners(leilao);
    }

    @Override
    public int getItemCount() {
        return leiloes.size();
    }


    private class ListItem extends RecyclerView.ViewHolder {
        TextView txtTitulo;
        TextView txtLance;
        TextView txtCriador;
        Button btnLance;
        ImageView img;

        private ListItem(View itemView) {
            super(itemView);

            txtTitulo = (TextView) itemView.findViewById(R.id.txtTitulo);
            txtLance = (TextView) itemView.findViewById(R.id.txtLance);
            txtCriador = (TextView) itemView.findViewById(R.id.txtCriador);
            btnLance = (Button) itemView.findViewById(R.id.btnLance);
            img = (ImageView) itemView.findViewById(R.id.imgItem);
        }

        private void bindOnClickListeners(final Leilao item) {
            btnLance.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickNovoLance(item);
                }
            });
            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ListItem.this.onSelectItem(item);
                }
            });
        }

        private void onSelectItem(Leilao leilao) {
            Intent intent = new Intent(context, LeilaoActivity.class);

            intent.putExtra("leilaoSelecionado", leilao);

            context.startActivity(intent);
        }
    }
}
