package br.com.a0.david2.gamessale;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import br.com.a0.david2.gamessale.Models.Usuario;
import br.com.a0.david2.gamessale.Rest.APIClient;
import br.com.a0.david2.gamessale.Rest.LoginAPI;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrarSeActivity extends AppCompatActivity {

    private TextView txtLogin;
    private TextView txtSenha;
    private TextView txtNome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_se);

        txtLogin = (TextView) findViewById(R.id.txtLogin);
        txtSenha = (TextView) findViewById(R.id.txtSenha);
        txtNome = (TextView) findViewById(R.id.txtNome);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(android.R.color.black));
        }
    }

    public void registrarUsuario(View v) {
        LoginAPI loginApi = APIClient.getLoginClient();
        Usuario usuario = new Usuario();

        String nome = txtNome.getText().toString();
        String login = txtLogin.getText().toString();
        String senha = txtSenha.getText().toString();

        usuario.setNome(nome);
        usuario.setEmail(login);
        usuario.setSenha(senha);

        Call<Usuario> call = loginApi.createUsuario(usuario);
        call.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                Log.d("MainActivity", "Status Code = " + response.code());

                if (response.isSuccessful()) {
                    Usuario usuario = response.body();

                    if (usuario.getMensagemDeErro() != null) {
                        showMensagemDeErro(usuario.getMensagemDeErro());
                    } else {
                        Intent intent = new Intent(RegistrarSeActivity.this, MainActivity.class);
                        salvarUsuarioLogado(usuario);
                        finish();
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Log.d("MainActivity", "Status Code = " + t);
            }
        });
    }

    public void salvarUsuarioLogado(Usuario usuario) {
        String spKey = getString(R.string.perf_key);
        SharedPreferences sp = getSharedPreferences(spKey, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.putString("usuarioLogado", new Gson().toJson(usuario));

        editor.apply();
    }

    public void showMensagemDeErro(String mensagem) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this)
                .setTitle("Erro de Login")
                .setMessage(mensagem)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                txtLogin.setText("");
                                txtSenha.setText("");
                            }
                        });

        alert.show();
    }
}
