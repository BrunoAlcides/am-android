package br.com.a0.david2.gamessale.Rest;

import br.com.a0.david2.gamessale.Models.Usuario;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by Bruno Alcides on 15/10/2017.
 */

public interface LoginAPI {
    @PUT("usuarios/login")
    Call<Usuario> login(@Body Usuario usuario);

    @POST("usuarios")
    Call<Usuario> createUsuario(@Body Usuario usuario);
}
