package br.com.a0.david2.gamessale.Rest;

import java.util.List;

import br.com.a0.david2.gamessale.Models.Plataforma;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Bruno Alcides on 22/10/2017.
 */

public interface PlataformaAPI {
    @GET("plataformas")
    Call<List<Plataforma>> getPlataformas();
}
