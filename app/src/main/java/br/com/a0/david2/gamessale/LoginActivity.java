package br.com.a0.david2.gamessale;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import br.com.a0.david2.gamessale.Models.Usuario;
import br.com.a0.david2.gamessale.Rest.APIClient;
import br.com.a0.david2.gamessale.Rest.LoginAPI;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText txtLogin, txtSenha;
    private LoginAPI loginApi;
    private ImageView imgLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtLogin = (EditText) findViewById(R.id.txtLogin);
        txtSenha = (EditText) findViewById(R.id.txtSenha);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        imgLogo = (ImageView) findViewById(R.id.imgLogo);

        setSupportActionBar(toolbar);

        loginApi = APIClient.getLoginClient();
    }

    @Override
    protected void onStart() {
        Picasso.with(getApplicationContext())
                .load(R.drawable.logo)
                .resize(imgLogo.getWidth(), 300)
                .into(imgLogo);
        super.onStart();
    }

    public void login(View v) {
        Usuario usuario = new Usuario();
        String login = txtLogin.getText().toString();
        String senha = txtSenha.getText().toString();

        usuario.setEmail(login);
        usuario.setSenha(senha);

        Call<Usuario> call = loginApi.login(usuario);
        call.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                Log.d("MainActivity", "Status Code = " + response.code());

                if (response.isSuccessful()) {
                    Usuario usuario = response.body();

                    if (usuario.getMensagemDeErro() != null) {
                        showMensagemDeErro(usuario.getMensagemDeErro());
                    } else {
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);

                        salvarUsuarioLogado(usuario);
                        finish();
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Log.d("MainActivity", "Status Code = " + t);
            }
        });
    }

    public void showMensagemDeErro(String mensagem) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this)
                .setTitle("Erro de Login")
                .setMessage(mensagem)
                .setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            txtLogin.setText("");
                            txtSenha.setText("");
                        }
                });

        alert.show();
    }

    public void salvarUsuarioLogado(Usuario usuario) {
        String spKey = getString(R.string.perf_key);
        SharedPreferences sp = getSharedPreferences(spKey, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.putString("usuarioLogado", new Gson().toJson(usuario));

        editor.apply();
    }

    public void irParaRegostrar(View v) {
        Intent intent = new Intent(this, RegistrarSeActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }
}
