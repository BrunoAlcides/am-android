package br.com.a0.david2.gamessale.Rest;

import java.util.List;

import br.com.a0.david2.gamessale.Models.Leilao;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Bruno Alcides on 17/10/2017.
 */

public interface LeilaoAPI {
    @GET("leiloes")
    Call<List<Leilao>> getLeiloes();

    @PUT("leiloes/{id}")
    Call<Leilao> atualizaLeilao(@Path("id") int leilaoId, @Body Leilao leilao);

    @PUT("leiloes/{id}/novolance")
    Call<Leilao> darNovoLance(@Path("id") int leilaoId, @Query("usuarioVencedorId") int usuarioVencedorId, @Query("novoLance") double novoLance);
}
