package br.com.a0.david2.gamessale.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Bruno Alcides on 17/10/2017.
 */

public class Categoria implements Serializable {

    @SerializedName("Codigo")
    private int codigo;

    @SerializedName("Titulo")
    private String titulo;

    public Categoria() {
    }

    public int getCodigo() {

        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Override
    public String toString() {
        return titulo;
    }
}
