package br.com.a0.david2.gamessale.Rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Bruno Alcides on 15/10/2017.
 */

public class APIClient {
    private static final String BASE_URL = "http://gamessaleam.azurewebsites.net/api/";
//    private static final String BASE_URL = "http://192.168.1.14:1000/api/" ;
    private static Retrofit client;
    private static LoginAPI loginClient;
    private static LeilaoAPI leilaoClient;
    private static CategoriaAPI categoriaClient;
    private static PlataformaAPI plataformaClient;

    public static Retrofit createClient() {
        if (client == null) {

            client = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        }

        return client;
    }

    public static LoginAPI getLoginClient() {
        if (loginClient == null) {
            Retrofit client = createClient();
            loginClient = client.create(LoginAPI.class);
        }
        return loginClient;
    }

    public static LeilaoAPI getLeilaoClient() {
        if (leilaoClient == null) {
            Retrofit client = createClient();
            leilaoClient = client.create(LeilaoAPI.class);
        }
        return leilaoClient;
    }

    public static CategoriaAPI getCategoriaClient() {
        if (categoriaClient == null) {
            Retrofit client = createClient();
            categoriaClient = client.create(CategoriaAPI.class);
        }
        return categoriaClient;
    }

    public static PlataformaAPI getPlataformaClient() {
        if (plataformaClient == null) {
            Retrofit client = createClient();
            plataformaClient = client.create(PlataformaAPI.class);
        }
        return plataformaClient;
    }
}
