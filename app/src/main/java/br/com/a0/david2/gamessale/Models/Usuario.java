package br.com.a0.david2.gamessale.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Bruno Alcides on 15/10/2017.
 */

public class Usuario implements Serializable {
    @SerializedName("Codigo")
    private int codigo;

    @SerializedName("Nome")
    private String nome;

    @SerializedName("Email")
    private String email;

    @SerializedName("Senha")
    private String senha;

    @SerializedName("Tipo")
    private int tipo;

    @SerializedName("ErroMensagemLogin")
    private String mensagemDeErro;

    public Usuario() {}

    public Usuario(int codigo, String nome, String email, String senha, int tipo, String mensagemDeErro) {
        this.codigo = codigo;
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.tipo = tipo;
        this.mensagemDeErro = mensagemDeErro;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getMensagemDeErro() {
        return mensagemDeErro;
    }

    public void setMensagemDeErro(String mensagemDeErro) {
        this.mensagemDeErro = mensagemDeErro;
    }
}
